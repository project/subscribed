<?php
// $Id$

function theme_subscribed_block_submit_button() {
	return '/' . drupal_get_path('module', 'subscribed') . '/images/block_submit.png';
}

/**
 * 
 */
function theme_subscribed_list_all($subscribed_list, $unsubscribed_list) {
  if (empty($subscribed_list) && empty($unsubscribed_list)) {
    return t('No subscriptions currently available.');
  }

  if (!empty($subscribed_list)) {
    foreach ($subscribed_list as $type => $subscriptions) {
      foreach ($subscriptions as $subscription) {
        $output_subscribed .= '<p>'. l($subscription->title, 'user/'. arg(1) .'/subscribe/'. $type) .'<br />'. $subscription->description .'</p>';
      }
      if (!empty($output_subscribed)) {
        $output_subscribed_list .= '<h3>'. $type .'</h3>'. $output_subscribed;
      }
      unset($output_subscribed);
    }
  }

  if (!empty($output_subscribed_list)) {
    $output = '<h2>'. t('Subscribed') .'</h2>'. $output_subscribed_list;
  }

  if (!empty($unsubscribed_list)) {
    foreach ($unsubscribed_list as $type => $subscriptions) {
      foreach ($subscriptions as $subscription) {
        $output_unsubscribed .= '<p>'. l($subscription->title, 'user/'. arg(1) .'/subscribe/'. $type) .'<br />'. $subscription->description .'</p>';
      }
      if (!empty($output_unsubscribed)) {
        $output_unsubscribed_list .= '<h3>'. $type .'</h3>'. $output_unsubscribed;
      }
      unset($output_unsubscribed);
    }

    $output .= (empty($output_subscribed_list)) ? '<h2>'. t('Subscriptions') .'</h2>' : '<h2>'. t('Also Available') .'</h2>';
    $output .= $output_unsubscribed_list;
  }

  return $output;
}

/**
 * 
 */
function theme_subscribed_log($form) {
  $date = $form['filter']['date']['#value'];

  switch ($form['filter']['period']['#value']) {
    case 1 :
      $sent = subscribed_log_day($date);
      $units = t('Hours');
      break;
    case 2 :
      $sent = subscribed_log_week($date);
      $units = t('Days');
      break;
    case 3 :
      $sent = subscribed_log_month($date);
      $units = t('Days');
      break;
    default :
      $sent = subscribed_log_day($date);
      $units = t('Hours');
      break;
  }

  $header[] = '<strong>'. $units .'</strong>';
  $row[] = array('data' => '<strong>'. t('Sent') .'</strong>', 'align' => 'left');

  while (list($unit_number, $sent_total) = each($sent)) {
    $header[] = $unit_number;
    $row[] = array(
      'data' => $sent_total,
      'align' => 'center'
    );
  }

  $rows[] = $row;

  $output = drupal_render($form);
  $output .= theme('table', $header, $rows, array('id' => 'subscribed-log'));

  return $output;
}

/**
 * 
 */
function theme_subscribed_overview($type, $label, $publication, $schedules) {
	$subscriber_total = 0;  
	$header = array(t('Schedule'), t('View'), t('Last Published'), t('Next Published'), t('Un-subscribers'), t('Subscribers'));

  foreach ($schedules as $schedule) {
    $schedule_title = l($schedule['schedule_title'], 'admin/subscribed/' . $type . '/schedule/'. $publication->publication_id .'/'. $schedule['schedule_id']);
    $preview = l(t('Preview'), 'admin/subscribed/' . $type . '/preview/'. $publication->publication_id .'/'. $schedule['schedule_id']);
    $last = ($schedule['last'] == 0) ? '-' : format_date($schedule['last'], 'small');
    $next = ($schedule['next'] == 0) ? t('manual') : format_date($schedule['next'], 'small');
		$unsubscribers = l(t('View'), 'admin/subscribed/' . $type . '/subscriptions/unsubscribed/' . $publication->publication_id . '/' . $schedule['schedule_id']);
    $subscriber_count = subscribed_count($type, $publication->publication_id, $schedule['schedule_id']);
		$subscriber_total = $subscriber_total + $subscriber_count;
    $subscribers = empty($subscriber_count) ? 0 : l($subscriber_count, 'admin/subscribed/' . $type . '/subscriptions/subscribed/' . $publication->publication_id . '/' . $schedule['schedule_id']); 

    $rows[] = array(
      array('data' => $schedule_title, 'align' => 'left'),
      array('data' => $preview, 'align' => 'left'),
      array('data' => $last, 'align' => 'left'),
      array('data' => $next, 'align' => 'left'),
      array('data' => $unsubscribers, 'align' => 'left'),
      array('data' => $subscribers, 'align' => 'left'),
      
    );
  }

	$subscriber_total = empty($subscriber_total) ? 0 : l($subscriber_total, 'admin/subscribed/' . $type . '/subscriptions/subscribed/' . $publication->publication_id);
	$unsubscriber_total = l(t('View'), 'admin/subscribed/' . $type . '/subscriptions/unsubscribed/' . $publication->publication_id);

  $rows[] = array(
    array('data' => ' ', 'colspan' => 3),  
    array('data' => '<strong>'. t('Total') .'</strong>', 'align' => 'left'),
    array('data' => $unsubscriber_total, 'align' => 'left'),
    array('data' => '<strong>'. $subscriber_total .'</strong>', 'align' => 'left')
  );

  $links = array(
    l(t('Send Now'), 'admin/subscribed/' . $type . '/send/'. $publication->publication_id),
    l(t('Add Schedule'), 'admin/subscribed/' . $type . '/schedule/'. $publication->publication_id),
    l(t('Set Content'), 'admin/subscribed/' . $type . '/content/'. $publication->publication_id),
    l(t('Delete'), 'admin/subscribed/' . $type . '/delete/'. $publication->publication_id),
    l(t('Reports'), 'admin/subscribed/' . $type . '/reports/'. $publication->publication_id),
  );
  
  $output .= '<div class="publication-controls">';
  $output .= '<h3>'. $publication->title .'</h3>';
  $output .= '<p>'. $publication->description .'</p>';
  $output .= theme('table', $header, $rows, array('id' => "publication"));
  $output .= theme('item_list', $links, NULL, 'ul', array('class' => 'links'));
  $output .= '</div>';

  return $output;
}

/**
 * For a given context, builds a formatted list of tokens and descriptions
 * of their replacement values.
 *
 * @param $type
 *   The token types to display documentation for. Defaults to 'all'.
 * @param $prefix
 *   The prefix your module will use when parsing tokens. Defaults to '['.
 * @param $suffix
 *   The suffix your module will use when parsing tokens. Defaults to ']'.
 * 
 * @return
 *   An HTML table containing the formatting docs.
 */
function theme_subscribed_token_help($type = 'all', $prefix = '[', $suffix = ']') {
  token_include();

  // See http://drupal.org/node/127072
  $full_list = array();
  foreach ((array)$type as $t) {
    $full_list = array_merge($full_list, token_get_list($t));
  }
  
  $headers = array(t('Token'), t('Replacement value'));
  $rows = array();
  foreach ($full_list as $key => $category) {
    $rows[] = array(array('data' => drupal_ucfirst($key) . ' ' . t('tokens'), 'class' => 'region', 'colspan' => 2));
    foreach ($category as $token => $description) {
      $row = array();
      $row[] = $prefix . $token . $suffix;
      $row[] = $description;
      $rows[] = $row;
    }
  }

  return theme('table', $headers, $rows, array('class' => 'description'));
}

function theme_subscribed_reports_page($reports, $chart) {

	$content .= '<div class="subscribed-report-chart">' . $chart . '</div>';
				
	foreach ($reports as $id => $report) {
		$content .=  theme_table($report['stats_header'], $report['stats'], array('id' => "subscribed-report-$id", 'class' => 'subscribed-report'), $report['title']);
	}
	
	return $content;
}
