<?php
// $Id:

/**
 *  Chart output 
 */
function subscribed_silverlight_chart($type, $id, $data) {
	$module_path = drupal_get_path('module', 'subscribed_silverlight');
	drupal_add_js($module_path .'/Visifire.js');
	$xap_file = '/' . $module_path . '/SL.Visifire.Charts.xap';
	$data = '/admin/reports/subscribed/' . $type . '/' . $id . '/data.xml';

	$chart = '<div id="VisifireChart1">
  						<script language="javascript" type="text/javascript">
            		var vChart = new Visifire("' . $xap_file . '",500,300);
      					vChart.setDataUri("' . $data . '"); // xml file name goes in the place of Data.xml
      					vChart.render("VisifireChart1");
  						</script>
						</div>';
 
	return $chart;
}



