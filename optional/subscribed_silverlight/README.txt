********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Subscribed Silverlight Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Displays subscribed and unsubscribed statistics as interactive Silverlight charts.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1 Download the Visifire for Silverlight 4 library from:

http://www.visifire.com

2 Extract the Visifire binary and move these files:

   /Silverlight Binaries/SL.Visifire.Charts.xap
   /Silverlight Binaries/Visifire.js

  To:

  /optional/subscribed_silverlight/

3. Place the entire Subscribed directory into your Drupal modules/
   directory.

4. Enable the module by navigating to:

     administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.

Steps 3 and 4 will be done as part of installing Subscribed module anyway, so 1 and 2 are the 
important steps here.


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/subscribed
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F




********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>

Sponsored by Microsoft.


