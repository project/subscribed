********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Subscribed Module
Author: Robert Castelo
Drupal: 6.x
********************************************************************
DESCRIPTION:

Handles data, logic, and UI of subscriptions(s).

The subscription can be used for most purposes, and includes following
information:

Type: Type of service being subscribed to - ties subscription to the module
using subscribed.module.

User ID: ID of user subscribed to publication/event/whatever

Publication ID: ID of publication/event being subscribed to

Subscribed: When subscription was started

Subscribed Terms: If a subscription is for a publication/event that allows 
subscription to a set of terms, selected terms can be subscribed to.  

Additional: 
Enable the Subscribed Silverlight module to see subscription statistics as a 
chart. Follow the instructions in /optional/subscribed_silverlight/README.txt 

This module provides an API, don't install it unless required by another module.



********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

1. Place the entire subscribed directory into your Drupal modules/
   directory.

2. Enable the module by navigating to:

     administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes.


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/subscribed
   
- Comission New Features:
   http://www.codepositive.com/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F




********************************************************************
ACKNOWLEDGEMENT

Developed by Robert Castelo for Code Positive <http://www.codepositive.com>


