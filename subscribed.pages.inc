<?php
// $Id$

/**
 * Display page, based on visitor's uid via $id_hash.
 */
function subscribed_proxy_page($node, $subscriber, $publication, $schedule, $action) {
  drupal_set_title(check_plain($node->title)); 

  $sql = "INSERT INTO {publications_track} (nid, uid, timestamp, publication_id, schedule_id, action_id, id_hash) VALUES (%d, %d, %d, %d, %d, %d, '%s')";
  db_query($sql, $node->nid, $subscriber->uid, time(), $publication->publication_id, $schedule->schedule_id, $action->action_id, $subscriber->id_hash);

  if (empty($node->body) && !empty($node->field_body)) {
    $node->body = $node->field_body;
  }
 
  $output = node_view($node);

  if (function_exists('comment_render') && $node->comment && function_exists('enewsletter_comment_render')) {
    $output .= enewsletter_comment_render($node, $subscriber);
  } 
  
  return $output;
}