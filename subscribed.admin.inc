<?php
// $Id$

/**
 * select which subscription types can be displayed as a subscription option
 */
function subscribed_configure_subscriptions() {
  // subscription types
  $types = subscribed_get_types();
  $options_description = (empty($types)) ? t('No modules have been enebled which allow subscriptions.') : t('Types of content to display as subscription options.');

  $options = array();

  foreach($types as $type => $info) {
		if (user_access('subscribe to '. $info['name'])) {
			$options[$type] = $info['name'];	
		}
  }
  
  $form['subscriptions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription Display'),
    '#description' => $options_description,
  );

  $form['subscriptions']['subscribed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Subscription Types'),
    '#default_value' => variable_get('subscribed_types_displayed', array()),
    '#options' => $options,
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Settings for all subscriptions.
 */
function subscribed_configure_subscriptions_submit($form, &$form_state) {
	menu_cache_clear_all();
	menu_router_build(TRUE);
  variable_set('subscribed_types_displayed', array_filter($form_state['values']['subscribed_types']));
  drupal_set_message(t('Subscription configuration settings saved.'));
}

/**
 *  configuration for standard registration form 
 */
function subscribed_configure_registration() {
  $type = arg(4);
  $settings = variable_get('subscribed_configure_type', array());
  $settings = $settings[$type];
  $form['#base'] = 'subscribed_configure'; 

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form['signup_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standard Registration Form'),
    '#description' => t('Options for standard registration page.'),
    '#weight' => -10,
  );
  $form['signup_options']['signup_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscription Box Title'),
    '#default_value' => $settings['signup_title'],
    '#size' => 30,
    '#maxlength' => 50,
  );
  $form['signup_options']['signup_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Subscription Box Message'),
    '#default_value' => empty($settings['signup_message']) ? t('Subscribe by filling in this form and clicking the button below.') : $settings['signup_message'],
  );
  $form['signup_options']['display_registration_options'] = array(
    '#type' => 'radios',
    '#title' => t('Single Subscription Options'),
    '#default_value' => empty($settings['display_registration_options']) ? 0 : $settings['display_registration_options'],
    '#options' => array(
      t('Optional - display subscription radio buttons'),
      t('Not Optional - no subscription radio buttons displayed')
    ),
    '#description' => t('When there is only one subscription available, should the registration form act as a sign up form, or should an option not to subscribe be shown.'),
  );
  $form['signup_options']['subscribed_default'] = array(
  	'#type' => 'checkbox',
  	'#title' => t('Subscribed as default'),
    '#description' => t('Set subscription option to "Subscribed" as the default.'),
    '#default_value' => $settings['subscribed_default'],
  );

  // hook_subscribed_configure_registration
  if ($extra = module_invoke_all('subscribed_configure_registration', $type, $settings)) {
    $form = array_merge_recursive($form, $extra);
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;	
}

/**
 *  configuration for subscription registration form (user/register/subscribed)
 */
function subscribed_configure_registration_subscription() {
  $type = arg(4);
  $settings = variable_get('subscribed_configure_type', array());
  $settings = $settings[$type];
  $form['#base'] = 'subscribed_configure'; 

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form['signup_options_subscribed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription Registration Form'),
    '#description' => t('Options for subscription registration page.'),
    '#weight' => -10,
  );
  $form['signup_options_subscribed']['signup_title_subscribed'] = array(
    '#type' => 'textfield',
    '#title' => t('Subscription Box Title'),
    '#default_value' => $settings['signup_title_subscribed'],
    '#size' => 30,
    '#maxlength' => 50,
  );
  $form['signup_options_subscribed']['signup_message_subscribed'] = array(
    '#type' => 'textarea',
    '#title' => t('Subscription Box Message'),
    '#default_value' => empty($settings['signup_message_subscribed']) ? t('Subscribe by filling in this form and clicking the button below.') : $settings['signup_message_subscribed'],
  );
  $form['signup_options_subscribed']['override_registration_help'] = array(
  	'#type' => 'checkbox',
  	'#title' => t('Override registration guidelines'),
  	'#default_value' => $settings['override_registration_help'],
  	'#description' => t('If this box is checked, registration guidelines will be replaced by the text you entered bellow.')
  );
  $form['signup_options_subscribed']['registration_help'] = array(
  	'#type' => 'textarea',
  	'#title' => t('Subscription registration guidelines'),
  	'#default_value' => $settings['registration_help'],
  	'#description' => t("This text is displayed at the top of the subscription registration form. It's useful for helping or instructing your users.")
  );
  
  // hook_subscribed_configure_registration_subscription
  if ($extra = module_invoke_all('subscribed_configure_registration_subscription', $type, $settings)) {
    $form = array_merge_recursive($form, $extra);
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;	
}

function subscribed_configure_misc() {
  $type = arg(4);
  $settings = variable_get('subscribed_configure_type', array());
  $settings = $settings[$type]; 
  $form['#base'] = 'subscribed_configure';

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  
  // hook_subscribed_configure_misc
  if ($extra = module_invoke_all('subscribed_configure_misc', $type, $settings)) {
    $form = array_merge_recursive($form, $extra);
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;	
}

/**
 * Display all newsletters.
 */
function subscribed_overview($type) {

	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$type];
  $publications = publication_select_publications($type);

  if (empty($publications)) {
    $output .= t('There are no %type.', array('%type' => $type_info['name']));
  }
  else {
    foreach ($publications as $publication) {
      $schedules = schedule_select_schedules($type, $publication->publication_id);
      $output .= theme('subscribed_overview', $type, $type_info['name'], $publication, $schedules);
    }
  } 

  return $output;
}

/**
 * 
 */
function subscribed_publication_add_form(&$form_state, $type, $type_label) {
  drupal_set_title(t('Add %type', array('%type' => $type_label)));

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form = module_invoke($type, 'subscribed_publication_add', $form);
  unset($form['relative']);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * 
 */
function subscribed_publication_add_form_validate($form, $form_state) {
  module_invoke($form_state['values']['type'], 'subscribed_publication_validate_add', $form, $form_state);
}

/**
 * 
 */
function subscribed_publication_add_form_submit($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == 'Cancel') {
    drupal_goto('admin/subscribed/' . $form_state['values']['type']);
  }

  $form_state['values']['publication_id'] = publication_insert_publication($form_state['values']['type'], $form_state['values']);
  module_invoke($form_state['values']['type'], 'subscribed_publication_add_submit', $form_state['values']['form_id'], $form_state['values']);
  drupal_set_message(t('Settings have been saved for "%title"', array('%title' => $form_state['values']['title'])));
  $form_state['redirect'] = 'admin/subscribed/' . $form_state['values']['type'];
  return;
}

/**
 *  
 */
function subscribed_template_add($type, $type_name) {
	drupal_goto('node/add/templates/'. $type);
}

function subscribed_preview($form_state, $publication_id, $schedule_id) {

  // Get previous settings.
  $publication = publication_select_publications(NULL, $publication_id);
  $roles_data = user_roles();

  // Remove 'anonymous user' option.
  foreach ($roles_data as $rid => $role_name) {
    if ($role_name != 'anonymous user') {
      $roles[$rid] = $role_name;
    }
  }

  $form['publication_id'] = array(
    '#type' => 'value',
    '#value' => $publication_id,
  );

  $form['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $schedule_id,
  ); 

  $form['recipients'] = array(
    '#type' => 'fieldset',
    '#title' => t('Recipients'),
    '#description' => t('Send to all users in these roles.'),
  ); 

  $form['recipients']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => $roles,
  ); 

  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content'),
    '#description' => t('Select a template to preview.'),
  ); 

  $form['content']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $publication->subject,
    '#size' => 30,
    '#maxlength' => 50,
    '#description' => t('Title of issue of publication.<br />Can be used: %day, %month, %dd, %mm, %yyyy'),
  );

  $templates_list = templates_get_templates($form['type']['#value']);
  $templates = array();

  foreach ($templates_list as $template) {
    $templates[$template->markup][$template->nid] = $template->title;
  }  

  $form['content']['template'] = array(
    '#type' => 'select',
    '#title' => t('Template'),
    '#default_value' => $template_selected,
    '#options' => $templates,
    '#required' => TRUE,
  );

  // Preview - cancel.
  $form['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * 
 */
function subscribed_preview_validate($form, &$form_state) {
	$values = $form_state['values'];

  if ($form_state['clicked_button']['#value'] == t('Cancel')) {
    drupal_goto($values['destination_cancel']);
  }
  if (empty($values['subject'])) {
    form_set_error('subject', t('A title must be set.'));
  }
}

/**
 * 
 */
function subscribed_preview_submit($form, &$form_state) {
  $values = $form_state['values'];

	/**
	 * @todo remove this dependancy and generalise feature
	 */ 
  $path = drupal_get_path('module', 'enewsletter') .'/enewsletter_send_receive.inc';
  include_once($path);

	global $base_url;

  // Get all users in roles.
  foreach ($values['roles'] as $rid => $include) {

    if (!empty($include)) {
      $result = db_query('SELECT * FROM {users_roles} WHERE rid = %d', $rid);
      while ($user = db_fetch_object($result)) {
        $users[$user->uid] = $user->uid;
      }
    }
  }

  if (empty($users)) {
    return drupal_set_message('No users in selected role(s).');
  }

  $send_count = 0;
  $site_name = check_plain(variable_get('site_name', 'drupal'));
  $publication = publication_select_publications(NULL, $values['publication_id']);
  $schedule = schedule_select_schedule($values['schedule_id']);
  $subject = publication_title_process($values['subject']);
  $subject = mime_header_encode($subject);
  
  $template_node = node_load($values['template']);
  $template['markup'] = $template_node->markup;
  $template['body'] = $template_node->body;
  $template['css'] = $template_node->css;
  
  $terms_allowed = publication_select_allowed_terms($publication->publication_id);

  // Get nodes.
  if (!empty($terms_allowed)) {
    $nodes_by_term = publication_get_nodes_and_terms($publication->type, $terms_allowed, $schedule['last'], $schedule['next']);
  }
  else {
    $nodes_by_term = publication_get_nodes($publication->type, $schedule['last'], $schedule['next']);
  }

  $terms_allowed = publication_terms_allowed_list($nodes_by_term);
  $nodes_allowed = publication_nodes_allowed_list($nodes_by_term);
	$action_id = schedule_action_select_db($publication->type, $publication->publication_id, $schedule['schedule_id']);

  foreach ($users as $uid) {
    // Get data for this user.
		/**
		 * @todo remove enewsletter dependancy
		 */ 
    $profile = enewsletter_load_profile($uid);
    $user_details = subscribed_subscription_user_details_select($uid);
    $user_details['uid'] = $uid;
    if (empty($user_details['hash'])) $user_details['hash'] = identity_hash_set_hash($uid); 
  
    $send['markup'] = $template_node->markup;
    $send['schedule_id'] = 1;   
    $forward = 'forward_email/'. $user_details['hash'] .'/'. $publication->publication_id .'/'. $send['schedule_id'] .'/0';
    $nodes = publication_process_nodes($nodes_allowed, $user_details['hash'], $send['markup'], $publication->publication_id, $send['schedule_id'], $action_id);
    $content = enewsletter_load_content($site_name, '', $publication, $template, $user_details, $forward, $profile, $nodes);

    $headers = array(
      'X-Newsletter' => $form_state['values']['publication_id'],
    );
   
    // Convert if subscription markup is plain text.
    if ($template_node->markup == 'html') {
      $content = preg_replace('/<a href="([^"]+)"[^>]*>(.+?)<\/a>/ie', 'html2txt_links_make_absolute("\\1", "\\2")', $content);
  			$content = preg_replace('/src="([^"]+)"/ie', 'html2txt_image_absolute("\\1")', $content);   
    }
    else {
      $converted = html2txt_convert($content, 70, FALSE);
      $content = $converted->text . $converted->links; 
    }
  
    $email_sent = deliver_send_email('subscribed-newsletter-preview', $user_details['mail'], $subject, $content, $template['markup'], $headers);

    if ($email_sent) {
      $send_count++;
    }
  }   

  drupal_set_message(t('%count email messages sent.', array('%count' => $send_count)));
}

/**
 * 
 */
function subscribed_send_now($form_state, $type, $publication_id) {

  $publication = publication_select_publications($type, $publication_id);
  $form = schedule_form_send_now($publication);
  $form['type'] = array('#type' => 'value',
												'#value' => check_plain($type),
												); 
												
  return $form;
}

/**
 * 
 */
function subscribed_send_now_validate($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Cancel')) { 
    drupal_goto('admin/subscribed/' . $form_state['values']['type']);
  }
}

/**
 * 
 */
function subscribed_send_now_submit($form, &$form_state) {

  $time = time();
  $schedules = schedule_update_send_now($form_state['values']['type'], $form_state['values']['publication_id'], $form_state['values']['schedules'], $time);
  subscribed_send_now_update_db($form_state['values']['type'], $form_state['values']['publication_id'], $schedules, $time);
  
  drupal_set_message(t("Publication time has been updated for %publication", array('%publication' => $form_state['values']['title'])));
  drupal_goto('admin/subscribed/' . $form_state['values']['type']);
}

function subscribed_reports_page($type, $publication_id) {

	drupal_set_title(t('!title Report', array('!title' => $publication->title)));
	$publication = publication_select_publications($type, $publication_id);
	$schedules = schedule_select_schedules($type, $publication->publication_id);
  
	// schedule stats
  foreach ($schedules as $schedule) {
    $subscribed = subscribed_count($type, $publication->publication_id, $schedule['schedule_id']);
    $unsubscribed = subscribed_count_unsubscribed($type, $publication->publication_id, $schedule['schedule_id']);
		$subscriber_total = $subscriber_total + $subscribed;
		$unsubscriber_total = $unsubscriber_total + $unsubscribed;
		$title = check_plain($schedule['schedule_title']);
			
    $subscriber_rows[] = array(
      array('data' => $title, 'align' => 'left'),
      array('data' => $subscribed, 'align' => 'left'),
    );

	  $unsubscriber_rows[] = array(
	    array('data' => $title, 'align' => 'left'),
	    array('data' => $unsubscribed, 'align' => 'left'),
	  );
  }

  $subscriber_rows[] = array(
    array('data' => t('Total'), 'align' => 'left'),
    array('data' => $subscriber_total, 'align' => 'left'),
  );

  $unsubscriber_rows[] = array(
    array('data' => t('Total'), 'align' => 'left'),
    array('data' => $unsubscriber_total, 'align' => 'left'),
  );

  $reports['subscribed']['title'] = t('Subscribed');
  $reports['subscribed']['stats_header'] = array(' ', ' '); 
  $reports['subscribed']['stats'] = $subscriber_rows;

  $reports['unsubscribed']['title'] = t('Unsubscribed');
  $reports['unsubscribed']['stats_header'] = array(' ', ' '); 
  $reports['unsubscribed']['stats'] = $unsubscriber_rows;

	$charts = module_invoke_all('subscribed_subscription_report_chart', $type, $publication_id, $data);

	if (is_array($charts)) {
		foreach ($charts as $chart) {
			$charts_output .= $chart;
		}
	} 

	return theme('subscribed_reports_page', $reports, $charts_output);
}

/**
 * Add new schedule.
 */
function subscribed_schedule_add($form_state, $type, $publication_id) {

	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$type]; 

  $publication = publication_select_publications($type, $publication_id);

  if (!isset($publication->publication_id)) {
    drupal_goto('admin/subscribed/' . $type);
  }

  $form['new_schedule'] = array(
    '#type' => 'value',
    '#value' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['publication_id'] = array(
    '#type' => 'value',
    '#value' => $publication_id,
  ); 
  $form['publication_title'] = array(
    '#value' => '<h2>'. t('!type: %title', array('!type' => $type_info['name'], '%title' => $publication->title)) .'</h2>',
  ); 

  schedule_form_admin($form, array('frequency', 'include', 'next_send'));
  templates_form_usage_admin($form);
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save Schedule'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  $form['#validate'][] = 'subscribed_schedule_add_validate';
	$form['#submit'][] = 'subscribed_schedule_add_submit';
  
  unset($form['relative']);
  
  return $form;
}

/**
 * Validate schedule.
 */
function subscribed_schedule_add_validate($form, &$form_state) {
	
	$values = $form_state['values'];  

  if ($form_state['clicked_button']['#value'] == t('Cancel')) {
    drupal_goto('admin/subscribed/' . $values['type']);
  } 

  schedule_validate_title($values);
  schedule_validate_frequency($values);
  schedule_validate_next_send($values);
  schedule_validate_included($values);
}

/**
 * Save schedule.
 */
function subscribed_schedule_add_submit($form, &$form_state) {
	$values = $form_state['values']; 
  $schedule_id = schedule_insert_schedule($values['type'], $values);
  templates_usage_set($values['templates'], 'schedule', $schedule_id);
  drupal_set_message(t('Schedule settings have been saved for "%title"', array('%title' => check_plain($values['schedule_title']))));
  $form_state['redirect'] = 'admin/subscribed/' . $values['type'];
}

/**
 *  Edit existing schedule
 */
function subscribed_schedule($form_state, $type, $publication_id = NULL, $schedule_id = NULL) {

  // Invalid operation.
  if (empty($publication_id) || !is_numeric($publication_id)) {
    drupal_goto('admin/subscribed/' . $type);
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['publication_id'] = array(
    '#type' => 'value',
    '#value' => $publication_id,
  );
  $form['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $schedule_id,
  );

  $schedule = schedule_select_schedule($schedule_id);
  $publication = publication_select_publications($type, $publication_id);  

  $form[] = array(
    '#value' => '<h2>'. t("'%publication' Schedule: %schedule", array('%publication' => $publication->title, '%schedule' => $schedule['schedule_title'])) .'</h2>',
  );
    
  $form['schedule_title'] = array(
    '#type' => 'value',
    '#value' => $schedule['schedule_title'],
  );

  schedule_form_admin($form);
  templates_form_usage_admin($form);
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Schedule'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  unset($form['relative']);
  return $form;
}


/**
 * 
 */
function subscribed_schedule_validate($form, &$form_state) {
	$values = $form_state['values']; 

  switch ($form_state['clicked_button']['#value']) {
    case t('Delete'): 
      // Check if this is the only schedule.
      $schedules = schedule_select_schedules($values['type'], $values['publication_id']);

      if (count($schedules) == 1) {
        drupal_set_message(t("'%schedule' could not be deleted because it is the only schedule available.", array('%schedule' => $values['schedule_title'])));   
        drupal_goto('admin/subscribed/' . $values['type']); 
      }

      drupal_goto('admin/subscribed/' . $values['type'] . '/schedule/delete/'. $values['publication_id'] .'/'. $values['schedule_id']);
    case t('Cancel'):
      drupal_goto('admin/subscribed/' . $values['type']); 
      break;
    case t('Save Schedule'):
      schedule_validate_title($values);
      schedule_validate_frequency($values);
      schedule_validate_next_send($values);
      schedule_validate_included($values);
      break;
  }

  return;
}

/**
 * 
 */
function subscribed_schedule_submit($form, &$form_state) {
	$values = $form_state['values'];
  schedule_update_schedule($values['type'], $values);
  subscribed_sends_static_update_db($values['type'], $values['publication_id'], $values['schedule_id']);
  templates_usage_set($values['templates'], 'schedule', $values['schedule_id']);
  drupal_set_message(t('Settings have been saved for "%title" schedule', array('%title' => $values['schedule_title'])));
  $form_state['redirect'] = 'admin/subscribed/' . $values['type']; 
}

/**
 * Menu callback: confirm wiping of the index.
 */
function subscribed_schedule_delete_confirm($form_state, $type, $publication_id = NULL, $schedule_id = NULL) {


  $form['publication_id'] = array(
    '#type' => 'value',
    '#value' => $publication_id,
  );
  $form['schedule_id'] = array(
    '#type' => 'value',
    '#value' => $schedule_id,
  );
    
  return confirm_form(
    $form, 
    t('Are you sure you want to delete this schedule?'),
    "admin/subscribed/$type/schedule/$publication_id/$schedule_id", 
    t('All subscriptions to this schedule will also be deleted.'), t('Delete'), t('Cancel')
  );
}

/**
 * 
 */
function subscribed_schedule_delete_confirm_validate($form, &$form_state) {
	$type = arg(2);
	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[arg(2)];
  // Check if this is the only schedule.
  $schedules = schedule_select_schedules($type, $form_state['values']['publication_id']);
  if (count($schedules) == 1) {
    drupal_set_message(t("'%schedule' could not be deleted because it is the only schedule for this !type.", array('%schedule' => check_plain($form_state['values']['schedule_title']), '!type' => $type_info['name'])));   
    drupal_goto('admin/subscribed/' . $type); 
  }
}

/**
 * Do the actual delete.
 */
function subscribed_schedule_delete_confirm_submit($form, &$form_state) {
	$type = arg(2); 
  schedule_delete($form_state['values']['schedule_id']);
  subscribed_subscriptions_deletion($type, NULL, $form_state['values']['publication_id'], $form_state['values']['schedule_id']);
  drupal_goto('admin/subscribed/' . $type);
}

/**
 * 
 */
function subscribed_subscribers($type, $status, $publication_id, $schedule_id = NULL, $pager_limit = 25) {
	
	// load reporting functions
	module_load_include('inc', 'subscribed', 'subscribed_report');
	 
	return subscribed_report_display($type, $status, $publication_id, $schedule_id, $pager_limit);
}

function subscribed_csv_export($type, $status, $publication_id, $schedule_id = NULL) { 

	// load reporting functions
	module_load_include('inc', 'subscribed', 'subscribed_report');
	
	subscribed_report_download($type, $status, $publication_id, $schedule_id);
	return;	
}

/**
 * Edit content of existing publication.
 */
function subscribed_content($form_state, $type, $publication_id) {

	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$type];
	
  $publication_settings = subscribed_publication_additional_configuration_get($publication_id);

	// Invalid operation.
  if (empty($publication_id) || !is_numeric($publication_id)) {
    drupal_goto('admin/subscribed/' . $type);
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  publication_form_admin($form, $publication_id);
	templates_form_usage_admin($form, FALSE);

	$form['email'] = array(
		'#type' => 'fieldset',
		'#title' => t('Email'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	); 
	
	$form['email']['subscription_welcome_subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Welcome Email Subject'),
		'#default_value' => $publication_settings->welcome_subject,
		'#description' => t('Customize the subject of the welcome e-mail, which is sent to new members upon registering on the subscription page of this publication.') .' '. t('Available variables are:') .' !username, !site, !password, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.',
		'#size' => 40,
		'#maxlength' => 255,
	);

	$form['email']['subscription_welcome_body'] = array(
		'#type' => 'textarea',
		'#title' => t('Welcome Email Body'),
		'#default_value' => $publication_settings->welcome_body,
		'#description' => t('Customize the body of the welcome e-mail, which is sent to new members upon registering on the subscription page of this publication.') .' '. t('Available variables are:') .' !username, !site, !password, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.',
		'#cols' => 80,
		'#rows' => 5,
	);
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save !type', array('!type' => $type_info['name'])),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
} 

/**
 * Validate newsletter.
 */
function subscribed_content_validate($form, &$form_state) {
	
	$values = $form_state['values'];
	
	if ($form_state['clicked_button']['#value'] == t('Cancel')) {
    drupal_goto('admin/subscribed/' . $values['type']);
  }

  publication_validate_content($values);
}

/**
 * Save newsletter.
 */
function subscribed_content_submit($form, &$form_state) {   

	$values = $form_state['values'];

  publication_update_publication($values);
	publication_set_allowed_terms($values);

	// save email settings	
  $publication_additional_configuration = subscribed_publication_additional_configuration_get($values['publication_id']);

 	if (empty($publication_additional_configuration)) {
	  db_query("INSERT INTO {subscribed_publication} (publication_id, welcome_subject, welcome_body) VALUES (%d, '%s', '%s')", $values['publication_id'], $values['subscription_welcome_subject'], $values['subscription_welcome_body']);
	} 
	else {
		db_query("UPDATE {subscribed_publication} SET welcome_subject = '%s', welcome_body = '%s' WHERE publication_id = %d LIMIT 1", $values['subscription_welcome_subject'], $values['subscription_welcome_body'], $values['publication_id']);
	}
	
	templates_default_set($values['default_markup'], $values['publication_id']);

  drupal_set_message(t('Content settings have been saved for "%title"', array('%title' => $values['title'])));
  $form_state['redirect'] = 'admin/subscribed/' . $values['type'];
}

/**
 * Delete publication.
 */
function subscribed_publication_deletion($form_state, $type, $publication_id) {

	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$type];
  
  $publication = publication_select_publications($type, $publication_id);

  if (!isset($publication)) {
    drupal_set_message(t("!type doesn't exist.", array('!type' => $type_info['name'])));
    return;
  }

  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form['publication_id'] = array(
    '#type' => 'value',
    '#value' => $publication->publication_id,
  );
  $form['title'] = array(
    '#type' => 'value',
    '#value' => $publication->title,
  );
  $form['question'] = array(
    '#value' => '<p>'. t('Are you sure you want to delete the email newsletter %title.', array('%title' => $publication->title)) .'</p>',
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete !type', array('!type' => $type_info['name'])),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Redirect user to overview page, if it clicks cancel.
 */
function subscribed_publication_deletion_validate($form, &$form_state) {
	$values = $form_state['values'];

  if ($form_state['clicked_button']['#value'] == t('Cancel')) {
    drupal_goto('admin/subscribed/' . $values['type']);
  }
}

/**
 * Do the actual delete.
 */
function subscribed_publication_deletion_submit($form, &$form_state) {
	$values = $form_state['values'];
	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$values['type']];
	
  publication_delete_publication($values['publication_id']);
  publication_delete_allowed_terms($values['publication_id']);
  subscribed_terms_deletion(NULL, NULL, $values['publication_id']);
  subscribed_subscriptions_deletion(NULL, NULL, $values['publication_id']);
  subscribed_send_deletion_by_publication('enewsletter', $values['publication_id']);
  subscribed_publication_config_delete($values['publication_id']);
  templates_default_delete($values['publication_id']);
  
  $schedules = schedule_select_schedules($values['type'], $values['publication_id']);
  
  foreach ($schedules as $schedule) {
    templates_usage_delete('schedule', $schedule['schedule_id']);
  }
  
  schedule_delete_publication_schedules($values['type'], $values['publication_id']);

  watchdog('special', 'The !type "%title" was deleted.', array('!type' => $type_info['name'], '%title' => $values['title']));
  drupal_set_message(t('The !type "%title" has been deleted.', array('!type' => $type_info['name'], '%title' => $values['title'])));
  drupal_goto('admin/subscribed/' . $values['type']);
}

/**
 * Provides a smart redirect, useful for adding to the login destination.
 *
 * @param Subscription type (string) $type 
 * @return none - causes a redirect.
 */ 
function subscribed_subscribe_redirect($type = NULL) {
	global $user;  
	
	if (empty($user->uid)) {
		$path = 'user/register/subscribed';
	} 
	else {
		$path = 'user/' . $user->uid . '/subscribe';
	}
	
	if (!empty($type)) $path .= "/$type";
	drupal_goto($path); 
}

/**
 * 
 */
function subscribed_log() {
  $types_available = db_fetch_array(db_query("SELECT DISTINCT type FROM {publications}"));

  if (empty($types_available)) {
    return print theme('page', t('Nothing available to subscribe to.'));
  }

  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('View Options'),
    '#collapsible' => TRUE,
  );

  $types[] = 'All';

  foreach ($types_available as $type) {
    $types[] = $type;
  }

  $form['filter']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#default_value' => variable_get('subscribed_log_type', NULL),
    '#options' => $types,
  );

  $form['filter']['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#default_value' => variable_get('subscribed_log_date', NULL),
  );

  $form['filter']['period'] = array(
    '#type' => 'select',
    '#title' => t('Period'),
    '#default_value' => variable_get('subscribed_log_period', NULL),
    '#options' => array(t('All'), t('Day'), t('Week')),
  );

  $form['filter']['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User'),
    '#default_value' => variable_get('subscribed_log_user', NULL),
    '#size' => 15,
    '#maxlength' => 11,
  );

  $form['filter'][] = array(
    '#type' => 'submit',
    '#value' => t('Show Sent'),
  );

  return $form;
}

/**
 * 
 */
function subscribed_log_submit($form, &$form_state) {
  variable_set('subscribed_log_type', $form_state['values']['type']);
  variable_set('subscribed_log_date', $form_state['values']['date']);
  variable_set('subscribed_log_period', $form_state['values']['period']);
  variable_set('subscribed_log_user', $form_state['values']['user']);
}