<?php
// $Id:

/**
 *  Display table of report results 
 */
function subscribed_report_display($type, $status, $publication_id, $schedule_id = NULL, $pager_limit = 25) {
	 
	/**
	 * @todo set these through UI 
	 */
	$cck_type = 'profile';
	$cck_fields = array('field_first_name', 'field_last_name', 'field_postcode'); 

	$header = subscribed_subscribers_data_header($status, $cck_type, $cck_fields, FALSE); 
	$subscribed_types_all = subscribed_get_types();
	$type_info = $subscribed_types_all[$type];
 
  $data = subscribed_subscribers_data($status, $publication_id, $schedule_id, $pager_limit);
	$data_rows = array();

  while ($subscriber = db_fetch_array($data)) {
		$data_rows[] = subscribed_report_row($subscriber, $status, $cck_fields);
  }

	foreach($data_rows as $row) {
    $row['name'] = l($row['name'], 'user/'. $row['uid'] .'/edit');
		$rows[] = array("data" => $row); 
	}

  $publication = publication_select_publications($type, $publication_id);
	$status_label = ($status == 'subscribed') ? t('Subscribed') : t('Unsubscribed');
	$title = t('@status to @publication', array('@status' => $status_label, '@publication' => $publication->title));
	
	if (!empty($schedule_id)) {	
		$schedule = schedule_select_schedule($schedule_id);
		$title .= ' (' . check_plain($schedule['schedule_title']) . ')';
	}
	
  drupal_set_title($title);

  if (empty($rows)) {
    return t('No records found.');
  }
  
	if (user_access('export ' . $type_info['name'] . ' (CSV)')) {
		$export_link = "admin/subscribed/$type/csv/$status/$publication_id";
		if (!empty($schedule_id)) $export_link .= "/$schedule_id";
		$output .= l(t('Export All @status (CSV)', array('@status' => $status_label)), $export_link);	
	}
	
  $output .= theme('table', $header, $rows, array('id' => $status));
  $output .= theme('pager', array(), $pager_limit);
  
  return $output;
}

/**
 *  Download a CSV file
 */
function subscribed_report_download($type, $status, $publication_id, $schedule_id = NULL) {

	// TO DO
	// set these through UI
	$cck_type = 'profile';
	$cck_fields = array('field_first_name', 'field_last_name', 'field_postcode');
	
	$new_line = "\r\n";
	
	if (strtolower($status) != 'subscribed' && $status != 'unsubscribed') drupal_not_found(); 
	$status_label = (strtolower($status) == 'subscribed') ? t('Subscribed') : t('Unsubscribed');

	// start file output
	drupal_set_header('Content-Type: text/x-comma-separated-values');
	drupal_set_header('Content-Disposition: attachment; filename="subscriptions-' . check_plain($status) . '.csv"');
	
	// output column headings
	$headings = subscribed_subscribers_data_header($status_label, $cck_type, $cck_fields);
	print implode(',', $headings) . $new_line;
	unset($headings);  
	
	// output rows of data
  $data = subscribed_subscribers_data($status, $publication_id, $schedule_id, 0); 

  while ($subscriber = db_fetch_array($data)) {            
	
		$row = subscribed_report_row($subscriber, $status, $cck_fields);
    $item = array();

    foreach ($row as $data_type => $cell) {
			// no need to filter some types of data      
			if ($data_type != 'uid' && $data_type != 'created' && $data_type != 'status' && $data_type != 'mail' && $data_type != 'name') {
				$cell = subscribed_csv_format($cell);
			}
			
			$item[] = $cell;
    }
    
    print implode(',', $item) . $new_line;
  }
	
	// end file output
  module_invoke_all('exit');
  exit;
}

/**
 * Create header for subscribers/unsubscribers table.
 *
 * @param $status
 *   'subscribed' or 'unsubscribed'.
 * @param $cck_type
 *   The CCK content type which fields belong to.
 * @param $formatter
 *   The field item to be formatted (such as $node->field_foo[0])
 *
 * @return
 *   An array of labels.
 */
function subscribed_subscribers_data_header($status, $cck_type, $cck_fields, $enclose = '"') {

	$status_label = (strtolower($status) == 'subscribed') ? t('Subscribed') : t('Unsubscribed'); 
  $header = array(t('UID'), t('Account Created'), $status_label); 
  //if (strtolower($status) == 'subscribed') $header = array_merge($header, array(t('Format'))); 
  $header = array_merge($header, array(t('Email'), t('User')));

	foreach ($cck_fields as $field_name) {
		$field = content_fields($field_name, $cck_type);
		$header[] = check_plain($field['widget']['label']);
	}
	
	if (empty($enclose)) {
		return $header;
	}
	
	foreach ($header as $column) {
		$headings[] = $enclose . $column . $enclose; 
	}
	
	return $headings;	
} 

/**
 * 
 */
function subscribed_subscribers_data($status, $publication_id, $schedule_id = NULL, $pager_limit = 25) {

	$table = (strtolower($status) == 'subscribed') ? 'subscribed' : 'subscribed_unsubscribed';
	$markup = (strtolower($status) == 'subscribed') ? ' s.markup,' : '';
	
  if ($schedule_id) {
		$sql = "SELECT s.uid,$markup s.$status, u.created, u.name, u.mail 
						FROM {$table} s 
						LEFT JOIN {users} u ON s.uid = u.uid 
						WHERE s.publication_id = %d AND s.schedule_id = %d 
						ORDER BY s.$status DESC, s.uid DESC";
						
		$args = array($publication_id, $schedule_id); 
		
	} else { 
		$sql = "SELECT s.uid,$markup s.$status, u.created, u.name, u.mail 
		FROM {$table} s 
		LEFT JOIN {users} u ON s.uid = u.uid 
		WHERE s.publication_id = %d 
		ORDER BY s.$status DESC, s.uid DESC";
		 
		$args = array($publication_id);
	}
	
	if ($pager_limit == 0) {
		$result = db_query($sql, $args);
	} 
	else {
		$result = pager_query($sql, $pager_limit, 0, NULL, $args);	
	}

  return $result;
}

/**
 * 
 */
function subscribed_report_row($subscriber, $status, $cck_fields = NULL) {
	
  $row['uid'] = $subscriber['uid'];  // uid
  $row['created'] = format_date($subscriber['created'], 'custom', 'd/m/Y'); // account creation date
  $row['status'] = format_date($subscriber[$status], 'custom', 'd/m/Y'); // subscribe or unsubscribe date
  //if (strtolower($status) == 'subscribed') $row['markup'] = array("data" => $subscriber['markup']); // format selected for subscription, e.g. HTML
  $row['mail'] = $subscriber['mail']; // email
  $row['name'] = $subscriber['name']; // username  

	// add CCK data
	$node = node_load(array('type' => 'profile', 'uid' => $subscriber['uid']), NULL, TRUE);

	foreach($cck_fields as $field_name) {
		$row[] = isset($node->{$field_name}[0]) ? content_format($field_name, $node->{$field_name}[0]) : NULL;
	} 

  return $row;
}

function subscribed_csv_format($data) {
  $data = preg_replace('/<.*?>/', '', $data); // strip html tags
  $data = str_replace(array("\r", "\n", ','), ' ', $data); // strip line breaks and commas
  $data = str_replace('"', '""', $data); // escape " characters
  $data = decode_entities($data);
  return '"' . $data . '"';
}