$(document).ready(function(){

  // clear input field on focus
  $("#edit-subscribed-email").focus(function(){
    $(this).val('');
  });

  // replace submit button with image
  $("#edit-subscribed-block-submit").attr('type', 'image').attr('src', Drupal.settings.submit_button);           
});